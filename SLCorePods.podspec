Pod::Spec.new do |s|
  s.name         = "SLCorePods"
  s.version      = "0.0.1"
  s.summary      = "Core CocoaPods to use across apps. Things like categories, libraries and common classes."
  s.license      = 'MIT'
  s.author       = {	
	"Mark McFarlane" => "mark@tacchistudios.com"
  }
  s.source       = { 
	:git => "git@bitbucket.org:tacchistudios/slcorepods.git", 
	:tag => "0.0.1"
  }
  s.platform     = :ios
  s.homepage     = "https://www.bitbucket.org/tacchistudios/slcorepods"

  s.dependency 'NSDate+Helper'

end